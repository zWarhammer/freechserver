require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.to_s + '/swagger'

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:to_swagger' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'Freech API V1',
        version: 'v1'
      },
      paths: {},
      securityDefinitions: {
        Bearer: {
          description: "...",
          type: :apiKey,
          name: 'Authorization',
          in: :header
        }
      },
      definitions: {
        User: {
          type: :object,
          properties: {
            user: {
              properties: {
                name: {
                  type: :string
                },
                email: {
                  type: :string
                },
                password: {
                  type: :string
                },
                username: {
                  type: :string
                },
                bio:{
                 type: :string
                },
              },
              required: [ 'name', 'email', 'password', 'username']
            }
          }
        },

        UserPushNotificationToken: {
            type: :object,
            properties: {
                user: {
                    properties: {
                        email: {
                            type: :string
                        },
                        push_token: {
                            type: :string
                        },
                    },
                    required: [ 'email', 'push_token']
                }
            }
        },

        SignUpResponse: {
          type: :object,
          properties: {
            message: {type: :string},
            auth_token: {type: :string}
          }
        },

        UserInfo: {
          type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            following: { type: :integer },
            follower: { type: :integer },
            username: { type: :string },
            bio:{ type: :string },
            email:{ type: :string },
            created_at:{ type: :string },
            updated_at:{ type: :string },
            posts:{ type: :integer },
            comments:{ type: :integer },
            likes:{ type: :integer },
            dislikes:{ type: :integer },
            groups:{ type: :integer },
            avatar:{ type: :string },
            follow:{ type: :string }
          }
        },

        Users: {
          type: :array,
          items: {
            type: :object,
            '$ref' => '#/definitions/UserInfo'
          }
        },

        Comment: {
          type: :object,
          properties: {
            body: { type: :string },
            user_id: { type: :integer },
            post_id: { type: :integer }
          }
        },

        Post: {
          type: :object,
          properties: {
            body: { type: :string },
            user_id: { type: :integer }
          }
        },

        Like: {
          type: :object,
          properties: {
            user_id: { type: :integer },
            post_id: { type: :integer }
          }
        },

        Dislike: {
          type: :object,
          properties: {
            user_id: { type: :integer },
            post_id: { type: :integer }
          }
        },

        Group: {
          type: :object,
          properties: {
            name: { type: :string },
            description: { type: :string },
            user_ids: {
              type: :array,
              items: { type: :integer }
            }
          }
        },

      }
    }
  }
end
