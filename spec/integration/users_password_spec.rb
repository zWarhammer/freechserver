require 'swagger_helper' 

describe 'Users API' do
  path '/password/forgot' do
    post 'Process forget password of user' do
      tags 'User Password'
      consumes 'application/json'
      operationId "forgot"
      parameter name: :body, 
                in: :body, 
                description: "User email",
                required: true,
                properties: {
                  email: {type: :string}
                }

      response '200', 'Token for forget password created successfully' do
        run_test!
      end

      response '404', 'Email address not found. Please check and try again.' do
        run_test!
      end
    end
  end

  path '/password/reset' do
    post 'Process reset password of user' do
      tags 'User Password'
      consumes 'application/json'
      operationId "reset"
      parameter name: :body, 
                in: :body, 
                description: "User email and password to reset",
                required: true,
                properties: {
                  email: {type: :string},
                  reset_password_token: {type: :string},
                  reset_password_sent_at: {type: :string},
                  new_password: {type: :string}
                }

      response '200', 'Token for forget password created successfully' do
        run_test!
      end

      response '404', 'Link not valid or expired. Try generating a new link.' do
        run_test!
      end
    end
  end

    path '/password/update' do
    patch 'Process update password of user' do
      tags 'User Password'
      consumes 'application/json'
      operationId "update"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "User email and password to update",
                required: true,
                properties: {
                  old_password: {type: :string},
                  new_password: {type: :string}
                }

      response '200', 'Updated password successfully' do
        run_test!
      end

      response '404', 'Not found' do
        run_test!
      end

      response '422', 'Unprocessable Entity' do
        run_test!
      end

    end
  end

end