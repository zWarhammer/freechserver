require 'swagger_helper' 

describe 'Likes API' do
  path '/likes' do
    post 'Create a new like' do
      tags 'Like'
      consumes 'application/json'
      operationId "createLike"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "Like object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Like'}

      response '201', 'Like created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    get "Get Current user's likes" do
      tags 'Like'
      produces 'application/json'
      operationId "getLikes"
      description "Gets all likes belonging to the Current User"
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Like'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    get 'Gets all likes' do
      tags 'Like'
      consumes 'application/json'
      operationId "showLikes"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Like'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/likes/{likeID}' do
    get 'Gets like by ID' do
      tags 'Like'
      produces 'application/json'
      operationId "getLike"
      description "Get like with matching ID"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :likeID,
                in: :path,
                type: :string,
                description: "ID of like to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Like'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    patch 'Update a like' do
      tags 'Like'
      consumes 'application/json'
      produces 'application/json'
      operationId "updateLike"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :likeID, 
                in: :path, 
                type: :string,
                description: "ID of like to update",
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be updated",
                required: true,
                schema: {'$ref' => '#/definitions/Like'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a like' do
      tags 'Like'
      consumes 'application/json'
      produces 'application/json'
      operationId "deleteLike"
      description "ID of like to delete"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :likeID, 
                in: :path, 
                type: :string,
                description: "ID of like to delete",
                required: true

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid like ID' do
        run_test!
      end
    end
  end
end