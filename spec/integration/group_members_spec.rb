require 'swagger_helper' 

describe 'Group members API' do
  path '/groups/member' do
    get "Gets Current user's groups (Membership)" do
      tags 'Group member'
      produces 'application/json'
      operationId "getGroupsMembership"
      description "Get all Groups that the Current User belongs to i.e Owns or is a Member of."
      security [Bearer: []]

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/groups/{id}/add_member' do
    patch "Add a group memeber" do
      tags 'Group member'
      consumes 'application/json'
      produces 'application/json'
      operationId "addGroupMember"
      description "Only send fields that should be updated; otherwise, remove them from the json. *Don't use this to change Group Name*"
      security [Bearer: []]
      parameter name: :id,
                in: :path,
                type: :string,
                description: "ID of group",
                required: true
      parameter name: :body,
                in: :body,
                required: true,
                schema: {'$ref' => '#/definitions/Group'}


      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end
  end

  path '/groups/{id}/remove_member' do
    patch "Remove a group memeber" do
      tags 'Group member'
      consumes 'application/json'
      produces 'application/json'
      operationId "removeGroupMember"
      description "Only send fields that should be updated; otherwise, remove them from the json. *Don't use this to change Group Name*"
      security [Bearer: []]
      parameter name: :id,
                in: :path,
                type: :string,
                description: "ID of group",
                required: true
      parameter name: :body,
                in: :body,
                required: true,
                schema: {'$ref' => '#/definitions/Group'}


      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end
  end

end