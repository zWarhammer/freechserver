require 'swagger_helper' 

describe 'Users API' do
  path '/current_user' do
    get 'Get current user profile' do
      tags 'User'
      consumes 'application/json'
      operationId "getCurrentUser"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/UserInfo'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/users' do
    post 'Create a new user' do
      tags 'User'
      consumes 'application/json'
      operationId "createUser"
      parameter name: :body, 
                in: :body, 
                description: "User object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/User'}

      response '201', 'Account created successfully' do
        schema '$ref' => '#/definitions/SignUpResponse'
        run_test!
      end

      response '422', 'Invalid input' do
        run_test!
      end
    end

    get 'Gets all users' do
      tags 'User'
      consumes 'application/json'
      operationId "showUsers"
      description "If have search parameter, return users matching to name or username partly"
      security [Bearer: []]
      parameter name: :page,
                in: :query,
                type: :int,
                required: false
      parameter name: :search,
                in: :query,
                type: :string,
                required: false
      response '200', 'Success' do
        schema '$ref' => '#/definitions/Users'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/users/{userID}' do
    patch 'Update a user' do
      tags 'User'
      consumes 'application/json'
      produces 'application/json'
      operationId "updateUser"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      tags 'User'
      produces 'application/json'
      parameter name: :userID, 
                in: :path, 
                type: :string,
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be Updated",
                required: true,
                schema: {'$ref' => '#/definitions/User'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a user' do
      tags 'User'
      consumes 'application/json'
      produces 'application/json'
      operationId "deleteUser"
      description "Id of user to delete"
      security [Bearer: []]
      tags 'User'
      produces 'application/json'

      parameter name: :userID, 
                in: :path, 
                type: :string,
                required: true


      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid user ID' do
        run_test!
      end
    end

    path '/users/{userID}/push_token' do
      post 'Set user token for push notification' do
        tags 'User'
        consumes 'application/json'
        produces 'application/json'
        operationId "push_tokenUser"
        description "Send user token push notification with email in json format"
        security [Bearer: []]
        tags 'User'
        produces 'application/json'
        parameter name: :userID,
                  in: :path,
                  type: :string,
                  required: true
        parameter name: :body,
                  in: :body,
                  description: "Fields to be updated",
                  required: true,
                  schema: {'$ref' => '#/definitions/UserPushNotificationToken'}

        response '200', 'Success' do
          run_test!
        end

        response '405', 'Invalid input' do
          run_test!
        end
      end
    end
  end
end