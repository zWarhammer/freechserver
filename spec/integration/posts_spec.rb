require 'swagger_helper' 

describe 'Posts API' do
  path '/posts' do
    post 'Create a new post' do
      tags 'Post'
      consumes 'application/json'
      operationId "createPost"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "Post object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Post'}

      response '201', 'Post created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    get "Get Current user's posts" do
      tags 'Post'
      produces 'application/json'
      operationId "getPosts"
      description "Gets all posts belonging to the Current User"
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    get 'Gets all posts' do
      tags 'Post'
      consumes 'application/json'
      operationId "showPosts"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/feed' do
    get 'Gets home feed' do
      tags 'Post'
      consumes 'application/json'
      operationId "feed"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/feed/recommend' do
    get 'Gets recommend feed' do
      tags 'Post'
      consumes 'application/json'
      operationId "feed_recommend"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/feed/group/{groupID}' do
    get 'Get feeds of a group' do
      tags 'Post'
      consumes 'application/json'
      operationId "feed_group"
      description ""
      security [Bearer: []]
      parameter name: :groupID,
                in: :path,
                type: :string,
                description: "ID of group to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/user_posts/{userID}' do
    get 'Get feeds of a user' do
      tags 'Post'
      consumes 'application/json'
      operationId "user_posts"
      description ""
      security [Bearer: []]
      parameter name: :userID,
                in: :path,
                type: :string,
                description: "ID of user to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/repost' do
    post 'Create a old post again' do
      tags 'Post'
      consumes 'application/json'
      operationId "repost"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "Post object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Post'}

      response '201', 'Post created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end
  end
  
  path '/posts/{postID}' do
    get 'Gets post by ID' do
      tags 'Post'
      produces 'application/json'
      operationId "getPost"
      description "Get post with matching ID"
      security [Bearer: []]
      parameter name: :postID,
                in: :path,
                type: :string,
                description: "ID of post to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Post'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    patch 'Update a post' do
      tags 'Post'
      consumes 'application/json'
      produces 'application/json'
      operationId "updatePost"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :postID, 
                in: :path, 
                type: :string,
                description: "ID of post to update",
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be updated",
                required: true,
                schema: {'$ref' => '#/definitions/Post'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a post' do
      tags 'Post'
      consumes 'application/json'
      produces 'application/json'
      operationId "deletePost"
      description "ID of post to delete"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :postID, 
                in: :path, 
                type: :string,
                description: "ID of post to delete",
                required: true

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid post ID' do
        run_test!
      end
    end

    path '/trending' do
      get 'GET top posts' do
        tags 'Post'
        consumes 'application/json'
        operationId "trending"
        description ""
        security [Bearer: []]

        response '200', 'Success' do
          schema '$ref' => '#/definitions/Post'
          run_test!
        end

        response '422', 'Unprocessable Entity: Missing Token' do
          run_test!
        end
      end
    end
  end
end