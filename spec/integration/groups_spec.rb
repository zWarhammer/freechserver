require 'swagger_helper' 

describe 'Groups API' do
  path '/groups' do
    post 'Create a new group' do
      tags 'Group'
      consumes 'application/json'
      operationId "createGroup"
      security [Bearer: []]
      parameter name: :body,
                in: :body, 
                description: "Group object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Group'}

      response '201', 'Group created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    get "Get Current user's groups" do
      tags 'Group'
      produces 'application/json'
      operationId "getGroups"
      description "Gets all groups belonging to the Current User"
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Group'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    get 'Gets all groups' do
      tags 'Group'
      consumes 'application/json'
      operationId "showGroups"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Group'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/groups/{groupID}' do
    get 'Gets group by ID' do
      tags 'Group'
      produces 'application/json'
      operationId "getGroup"
      description "Get group with matching ID"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :groupID,
                in: :path,
                type: :string,
                description: "ID of group to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Group'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    patch 'Update a group' do
      tags 'Group'
      consumes 'application/json'
      produces 'application/json'
      operationId "updateGroup"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :groupID, 
                in: :path, 
                type: :string,
                description: "ID of group to update",
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be updated",
                required: true,
                schema: {'$ref' => '#/definitions/Group'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a group' do
      tags 'Group'
      consumes 'application/json'
      produces 'application/json'
      operationId "deleteGroup"
      description "ID of group to delete"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :groupID, 
                in: :path, 
                type: :string,
                description: "ID of group to delete",
                required: true

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid group ID' do
        run_test!
      end
    end
  end
end