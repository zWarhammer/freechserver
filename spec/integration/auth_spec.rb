require 'swagger_helper' 

describe 'Auth API' do
  path '/auth/login' do
    post 'Logs user into the system' do
      tags 'Auth'
      produces 'application/json'
      consumes 'application/json'
      operationId "loginUser"
      parameter name: :email,
                in: :query,
                type: :string,
                required: true,
                description: 'User email address'
      parameter name: :password,
                in: :query,
                type: :string,
                required: true,
                description: 'User password'

      # parameter name: :body,
      #           in: :body,
      #           description: "Login info",
      #           required: true,
      #           properties: {
      #               email: {type: :string},
      #               password: {type: :string}
      #           }


      response '200', 'Success' do
        schema type: :object,
          properties: {
            auth_token: { type: :string }
          }
        run_test!
      end

      response '400', 'Invalid email/password' do
        run_test!
      end

      response '401', 'Invalid credentials' do
        schema type: :object,
          properties: {
            message: { type: :string }
          }
        run_test!
      end
    end
  end
end