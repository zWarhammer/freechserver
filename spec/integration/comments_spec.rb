require 'swagger_helper' 

describe 'Comments API' do
  path '/comments' do
    post 'Create a new comment' do
      tags 'Comment'
      consumes 'application/json'
      operationId "createComment"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "Comment object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Comment'}

      response '201', 'Comment created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    get "Get Current user's comments" do
      tags 'Comment'
      produces 'application/json'
      operationId "getComments"
      description "Gets all comments belonging to the Current User"
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Comment'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    get 'Gets all comments' do
      tags 'Comment'
      consumes 'application/json'
      operationId "showComments"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Comment'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/comments/{commentID}' do
    get 'Gets comment by ID' do
      tags 'Comment'
      produces 'application/json'
      operationId "getComment"
      description "Get comment with matching ID"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :commentID,
                in: :path,
                type: :string,
                description: "ID of comment to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Comment'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    patch 'Update a comment' do
      tags 'Comment'
      consumes 'application/json'
      produces 'application/json'
      operationId "updateComment"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :commentID, 
                in: :path, 
                type: :string,
                description: "ID of comment to update",
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be updated",
                required: true,
                schema: {'$ref' => '#/definitions/Comment'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a comment' do
      tags 'Comment'
      consumes 'application/json'
      produces 'application/json'
      operationId "deleteComment"
      description "ID of comment to delete"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :commentID, 
                in: :path, 
                type: :string,
                description: "ID of comment to delete",
                required: true

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid comment ID' do
        run_test!
      end
    end
  end
end