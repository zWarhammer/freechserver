require 'swagger_helper' 

describe 'Disdislikes API' do
  path '/dislikes' do
    post 'Create a new dislike' do
      tags 'Dislike'
      consumes 'application/json'
      operationId "createDislike"
      security [Bearer: []]
      parameter name: :body, 
                in: :body, 
                description: "Dislike object to be created",
                required: true,
                schema: {'$ref' => '#/definitions/Dislike'}

      response '201', 'Dislike created successfully' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    get "Get Current user's dislikes" do
      tags 'Dislike'
      produces 'application/json'
      operationId "getDisdislikes"
      description "Gets all dislikes belonging to the Current User"
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Dislike'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    get 'Gets all dislikes' do
      tags 'Dislike'
      consumes 'application/json'
      operationId "showDisdislikes"
      description ""
      security [Bearer: []]

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Dislike'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end
  end

  path '/dislikes/{dislikeID}' do
    get 'Gets dislike by ID' do
      tags 'Dislike'
      produces 'application/json'
      operationId "getDislike"
      description "Get dislike with matching ID"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :dislikeID,
                in: :path,
                type: :string,
                description: "ID of dislike to get",
                required: true

      response '200', 'Success' do
        schema '$ref' => '#/definitions/Dislike'
        run_test!
      end

      response '422', 'Unprocessable Entity: Missing Token' do
        run_test!
      end
    end

    patch 'Update a dislike' do
      tags 'Dislike'
      consumes 'application/json'
      produces 'application/json'
      operationId "updateDislike"
      description "Only send fields that should be updated; otherwise, remove them from the json"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :dislikeID, 
                in: :path, 
                type: :string,
                description: "ID of dislike to update",
                required: true
      parameter name: :body,
                in: :body,
                description: "Fields to be updated",
                required: true,
                schema: {'$ref' => '#/definitions/Dislike'}

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid input' do
        run_test!
      end
    end

    delete 'Delete a dislike' do
      tags 'Dislike'
      consumes 'application/json'
      produces 'application/json'
      operationId "deleteDislike"
      description "ID of dislike to delete"
      security [Bearer: []]
      produces 'application/json'
      parameter name: :dislikeID, 
                in: :path, 
                type: :string,
                description: "ID of dislike to delete",
                required: true

      response '200', 'Success' do
        run_test!
      end

      response '405', 'Invalid dislike ID' do
        run_test!
      end
    end
  end
end