FactoryBot.define do
  factory :flag do
    user nil
    reason "MyString"
    type ""
    flagged 1
  end
end
