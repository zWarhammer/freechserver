FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }    
    name  { Faker::Name.name }
    username { Faker::Name.unique.name_with_middle }
  end
end
