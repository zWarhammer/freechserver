class AddPublicToGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :groups, :inviteonly, :boolean
  end
end
