class CreateBlocks < ActiveRecord::Migration[5.2]
  def change
    create_table :blocks do |t|
      t.belongs_to :user, index: true
      t.integer :block, index: true

      t.timestamps
    end
  end
end
