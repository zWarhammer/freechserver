class DropUserGroups < ActiveRecord::Migration[5.2]
  def down
    drop_table :user_groups
  end
end
