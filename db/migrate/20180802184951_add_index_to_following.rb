class AddIndexToFollowing < ActiveRecord::Migration[5.2]
  def change
    add_index :following, :user_id
    add_index :following, :following_id
  end
end
