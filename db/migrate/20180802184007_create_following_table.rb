class CreateFollowingTable < ActiveRecord::Migration[5.2]
  def change
    create_table :following do |t|
      t.integer :user_id
      t.integer :following_id
    end
  end
end
