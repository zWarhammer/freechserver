class FixTypeNameOnFlag < ActiveRecord::Migration[5.2]
  def change
    rename_column :flags, :type, :contenttype
  end
end
