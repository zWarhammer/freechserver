class AddOwnerToGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :groups, :owner_id, :integer
    add_index :groups, :owner_id
  end
end
