class AddFollowToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :follow, :boolean
  end
end
