class CreateFlags < ActiveRecord::Migration[5.2]
  def change
    create_table :flags do |t|
      t.belongs_to :user, foreign_key: true
      t.string :reason
      t.string :type
      t.integer :flagged

      t.timestamps
    end
  end
end
