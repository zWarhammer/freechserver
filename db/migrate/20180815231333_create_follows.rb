class CreateFollows < ActiveRecord::Migration[5.2]
  def change
    create_table :follows do |t|
      t.integer :user_id
      t.integer :follow_id
      t.index ["follow_id"], name: "index_follows_on_follow_id"
      t.index ["user_id"], name: "index_follows_on_user_id"
      t.timestamps
    end
  end
end
