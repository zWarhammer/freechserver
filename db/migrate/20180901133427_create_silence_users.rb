class CreateSilenceUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :silence_users do |t|
      t.references :group, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :silence_time

      t.timestamps
    end
  end
end
