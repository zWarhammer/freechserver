class PushNotification
  def self.getRelatedPushTokens(user)
    group_ids = Group.joins(:users).where(users: {id: user.id})

    # Get owner users
    group_owner_ids = Group.where(id: group_ids).pluck(:owner_id)

    # Get group members
    group_member_ids = Group.joins(:users).where(groups: {id: group_ids}).pluck(:user_id)

    # Get followers
    follower_ids = Follower.where(user_id: user.id).pluck(:follower_id)

    # Merge owners and member users
    target_user_ids = group_owner_ids | group_member_ids | follower_ids

    # Get push tokens for group users
    User.where(id: target_user_ids).where.not(push_token: nil).pluck(:push_token)
  end

  def self.notifyPost(user, post)
    # Get push tokens for group users
    push_tokens = self.getRelatedPushTokens(user)
    notification_message = "#{user.name} posted \"#{post.body ? post.body.truncate(25) : ''}\" on #{post.created_at.to_s} "
    data = post ? post.to_json : nil
    # Send users a notification for post
    self.sendToMultiUsers(push_tokens, notification_message, data) if push_tokens.size > 0
  end

  def self.notifyRepost(user, post)
    # Get push tokens for group users
    push_tokens = self.getRelatedPushTokens(user)
    notification_message = "#{user.name} re-posted \"#{post.body ? post.body.truncate(25) : ''}\" on #{post.created_at.to_s} "
    data = post ? post.to_json : nil
    # Send users a notification for post
    self.sendToMultiUsers(push_tokens, notification_message, data) if push_tokens.size > 0
  end

  def self.notifyComment(user, comment)
    post = comment.post
    posted_user = post.user
    notification_message = "#{user.name} left a comment to your post \"#{post.body ? post.body.truncate(25) : ''}\" on #{post.created_at.to_s} "
    data = post ? post.to_json : nil
    self.sendToUser(posted_user.push_token, notification_message, data) if posted_user.push_token
  end

  def self.notifyLike(user, like)
    post = like.post
    posted_user = post.user
    notification_message = "#{user.name} liked your post \"#{post.body ? post.body.truncate(25) : ''}\" on #{post.created_at.to_s} "
    data = post ? post.to_json : nil
    self.sendToUser(posted_user.push_token, notification_message, data) if posted_user.push_token
  end

  def self.notifyDislike(user, dislike)
    post = dislike.post
    posted_user = post.user
    notification_message = "#{user.name} disliked your post \"#{post.body ? post.body.truncate(25) : ''}\" on #{post.created_at.to_s} "
    data = post ? post.to_json : nil
    self.sendToUser(posted_user.push_token, notification_message, data) if posted_user.push_token
  end

  def self.notifyFollow(following, follower)
    following_user = following.user
    follower_user = follower.user
    notification_message = "#{following_user.name} followed you"
    data = following_user.as_json(:only => [:id, :name, :email, :username, :avatar_content_type, :avatar_file_name, :avatar_file_size])
    self.sendToUser(follower_user.push_token, notification_message, data) if follower_user.push_token
  end

  def self.notifyUnfollow(following, follower)
    following_user = following.user
    follower_user = follower.user
    notification_message = "#{following_user.name} unfollowed you"
    data = following_user.as_json(:only => [:id, :name, :email, :username, :avatar_content_type, :avatar_file_name, :avatar_file_size])
    self.sendToUser(follower_user.push_token, notification_message, data) if follower_user.push_token
  end

  def self.sendToUser(push_token, message, data = nil, sound = 'default', badge_count = 0)
    client = Exponent::Push::Client.new

    messages = []
    msg = {
        to: push_token,
        body: message,
    }
    msg.merge!(sound: sound) if sound
    msg.merge!(badge_count: badge_count) if badge_count > 0
    msg.merge!(data: data) if data
    messages << msg

    client.publish messages
  end

  def self.sendToMultiUsers(push_tokens, message, data=nil, sound = 'default', badge_count = 0)
    client = Exponent::Push::Client.new

    messages = []
    push_tokens.each do |push_token|
      msg = {
          to: push_token,
          body: message
      }
      msg.merge!(sound: sound) if sound
      msg.merge!(badge_count: badge_count) if badge_count > 0
      msg.merge!(data: data) if data
      messages << msg
    end

    client.publish messages
  end
end