class Post < ApplicationRecord
  has_attached_file :image, :styles => { :medium => "300x150>", :thumb => "100x50#" }, :default_url => ''
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  # mount_uploader :image, ImageUploader
  # serialize :image, JSON
  belongs_to :user
  belongs_to :group, optional: true
  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy
  has_many :dislikes, :dependent => :destroy
  #Scopes
  scope :noprivate, -> { includes(:group).where("groups.inviteonly = ? OR group_id IS NULL",false).references(:groups) }
  scope :trending, -> { noprivate.order('score DESC') }
  scope :recommend, -> {noprivate.trending }

  def self.from_users_followed_by(user)
    followed_user_ids = "SELECT following_id FROM followings
                         WHERE user_id = :user_id"
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id",
          user_id: user.id)
  end

  def reference
  	if Post.exists? reference_post
  		Post.find reference_post
  	else
  		nil
  	end
  end

end
