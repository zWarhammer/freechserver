class Flag < ApplicationRecord
  belongs_to :user
  validates :contenttype, :flagged, :reason, presence: true
  validates_uniqueness_of :flagged, scope: :user_id
end
