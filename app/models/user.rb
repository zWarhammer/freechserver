class UniqueValidator < ActiveModel::Validator
  def validate(record)
    if record.check_unique_user == "username"
      record.errors[:base] << "Username already taken"
    elsif record.check_unique_user == "email"
      record.errors[:base] << "Email already taken"
    end
  end
end

class User < ApplicationRecord
  #Encrypt Password
  validates_presence_of :name, :email
  has_secure_password
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :dislikes, dependent: :destroy
  has_many :following, dependent: :destroy
  has_many :follower, dependent: :destroy
  has_many :blocks, dependent: :destroy
  has_and_belongs_to_many :groups

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/user_avatar.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  # mount_uploader :avatar, ImageUploader
  # serialize :avatar, JSON


  validates_uniqueness_of :email, on: :create
  validates_uniqueness_of :username, on: :create

  validates_with UniqueValidator, on: :update

  serialize :group_owner, Array

  def feed
    Post.from_users_followed_by(self)
  end

  def following?(following)
    self.following.find_by(following_id: following)
  end

  def self.find_or_create_from_auth_hash(auth_hash)
  	user = find_by(email: auth_hash[:email])
  	if user.nil?
  		user = create(email: auth_hash[:email], password_digest: "password_digest" )
  	end
    user.update(
      provider: auth_hash[:provider],
      uid: auth_hash[:uid],
      name: auth_hash[:name],
      username: auth_hash[:username],
      auth_token: auth_hash[:token],
      auth_secret: auth_hash[:secret]
    )
    user
  end

  #TODO - Make more simple.  Just query for user with user name, if it is exists, fail it.
  def check_unique_user
    @users = User.all

    counts = Hash.new(0)
    @users.each do |user|
      counts[user.username] += 1
      counts[user.email] += 1
    end

    @users.each do |user|
      if user.id == self.id
          if counts[self.username.downcase] > 1
            return "username"
          elsif counts[self.email.downcase] > 1
            return "email"
          end
      else
        if self.username.downcase == user.username.downcase
          return "name"
        elsif self.email.downcase == user.email.downcase
          return "email"
        end
      end
    end

  end

  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.now.utc
    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.reset_password_sent_at = nil
    self.confirmed_at = Time.now.utc
    self.password = password
    save!
  end

  private

  def generate_token
    SecureRandom.hex(10)
  end

end
