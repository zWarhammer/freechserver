class Block < ApplicationRecord
  belongs_to :user
  validates :user_id, :block, presence: true
  validates_uniqueness_of :block, scope: :user_id
end
