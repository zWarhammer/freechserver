class UniqueValidator2 < ActiveModel::Validator
  def validate(record)
    if record.check_unique_group
      record.errors[:base] << "Group Name already taken"
    end
  end
end

class Group < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :posts, :dependent => :destroy
  validates_presence_of :name
  validates_uniqueness_of :name, on: :create
  has_many :banned_users, :dependent => :destroy
  has_many :silence_users, :dependent => :destroy

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/group_avatar.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  # mount_uploader :avatar, ImageUploader
  # serialize :avatar, JSON
  validates_with UniqueValidator2, on: :update

  def check_unique_group
    @groups = Group.all

    counts = Hash.new(0)
    @groups.each do |group|
      counts[group.name.downcase] += 1
    end

    @groups.each do |group|
      if group.id == self.id
          if counts[self.name.downcase] > 1
            return true
          end
      else
        if self.name.downcase == group.name.downcase
          return true
        end
      end
    end

    return false
  end

end
