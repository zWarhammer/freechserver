class Following < ApplicationRecord
  belongs_to :user
  validates :user_id, :following_id, presence: true
end
