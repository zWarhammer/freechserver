class FollowsController < ApplicationController
  # POST /follows
  def create
    #The logic is that if any of the follow users are from the current user,
    #don't allow them to follow again
    @fid=params["follow_id"]
    @user=current_user
    #Check if any are in the db already..
    @z=@user.follows.where(follow_id: [@fid])
    if @z.count > 0
        json_response({ status: 'Current User is already following this user'})
        return;
    end

    @follow = Follow.new 
    @follow.user_id = current_user
    @follow.follow_id = @fid

    if @follow.save
      json_response({ status: 'Success'})
    else
      render json: @follow.errors, status: :unprocessable_entity
    end
  end

  # DELETE /likes/1
  def destroy
    @follow.destroy
  end

end
