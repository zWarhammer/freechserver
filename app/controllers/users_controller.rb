class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy, :follow, :unfollow, :followings, :followers]
  skip_before_action :authorize_request, only: :create

  # GET /followings/:id
  def followings
    @followings = @user.id == current_user.id ? current_user.following : @user.following
    if @followings
      @following = []
      @followings.each.with_index do |user, index|
        @following << User.find(user.following_id)
        @follow = current_user.following?(user.following_id)
        @following[index].follow = @follow
      end
      render json: @following, each_serializer: UserMinSerializer
    else
      json_response(message: 'No followings')
    end
  end

  # GET /followers/:id
  def followers
    @followers = @user.id == current_user.id ? current_user.follower : @user.follower
    if @followers
      @follower = []
      @followers.each.with_index do |user, index|
        @follower << User.find(user.follower_id)
        @follow = current_user.following?(user.follower_id)
        @follower[index].follow = @follow
      end
      render json: @follower, each_serializer: UserMinSerializer
    else
      json_response({ message: 'No followers'})
    end
  end

  # PATCH /follow/:id
  def follow
    if current_user.id === @user.id
      json_response(message: 'You cant following yourself')
    elsif current_user.following.exists?(following_id: @user.id)
      json_response(message: 'You follow this user.')
    else
      @following = current_user.following.build
      @following.following_id = @user.id
      if @following.save
        @follower = @user.follower.build
        @follower.follower_id = current_user.id
        # @user.update(follow: true)
        if @follower.save
          PushNotification.notifyFollow(@following, @follower)
          render json: @user
        else
          json_response(message: 'Something wrong.')
        end
      end
    end
  end

  # PATCH /unfollow/:id
  def unfollow
    if current_user.id === @user.id
      json_response(message: 'You cant following yourself')
    elsif current_user.following.exists?(following_id: @user.id)
      @following = current_user.following.find_by_user_id(current_user.id)
      @follower = @user.follower.find_by_follower_id(current_user.id)
      PushNotification.notifyUnfollow(@following, @follower)
      @following.destroy
      @follower.destroy
      @user.update(follow: false)

      render json: @user
    else
      json_response(message: 'You do not follow this user.')
    end
  end

  # GET /users
  def index
    if params[:search].present?
      if params[:search].size == 1 && params[:search].first == '@'
        @users = User.all.order(name: :asc).paginate(page: params[:page], per_page: 10)
      else
        search_condition = params[:search].first == '@' ? "#{params[:search][1, (params[:search].size - 1)]}%" : "%#{params[:search]}%"
        @users = User.where("name ILIKE ? OR username ILIKE ?", search_condition, search_condition).paginate(page: params[:page], per_page: 10)
      end
    else
      @users = User.all.order(name: :asc).paginate(page: params[:page], per_page: 10)
    end

    render json: @users
  end

  # GET /users/1
  def show
    @user.follow = current_user.following?(@user.id)
    render json: @user
  end

  # GET /current_user
  def current
    render json: current_user
  end

  # POST /users
  def create
    @user = User.new(user_params)
    @user.email.downcase!

    if @user.save
      auth_token = AuthenticateUser.new(@user.email, @user.password).call
      response = { message: Message.account_created, auth_token: auth_token }
      json_response(response, :created)
      # render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  # POST /users/1/push_token
  def push_token
    set_user
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # GET /users/search
  def search
    @users = User.where("name LIKE '%?%' OR username LIKE '%?%'", params[:search], params[:search])
                 .order(name: :asc).paginate(page: params[:page], per_page: 10)
    render json: @users
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :auth_token, :name, :username, :bio, :avatar, :push_token)
    end
end
