module V1
  class GroupsController < ApplicationController

    before_action :set_group, only: [:show, :update, :destroy]

  # GET /groups
  def index
    # @Group = Group #for some reason this fixes undefined method 'read attributes for serialization'
    @groups = Group.where(owner_id: current_user.id).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    # @groups = current_user.group_owner
    render json: @groups, each_serializer: GroupMinSerializer
  end

  # GET /groups/1
  def show
    render json: @group, each_serializer: GroupSerializer
  end

  # GET /groups/member
  def member
    @groups = Group.joins(:users).where(users: { id: current_user.id }).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    # @groups = current_user.groups.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    render json: @groups, each_serializer: GroupMinSerializer
  end

  #POST /groups/1/add_member
  def add_member
    #The logic is to call the update function with all the current members plus the new member of the group
    @group = Group.find(params["id"].to_i)
    if @group.owner_id == current_user.id
      @add_member_params = {"id" => @group.id, "user_ids" => []}

      @current_member_ids = @group.user_ids
      @current_member_ids.each do |current_member_id|
        @add_member_params["user_ids"] << current_member_id
      end

      @to_be_added = group_params["user_ids"]
      @to_be_added.each do |user|
        @add_member_params["user_ids"] << user.to_i
      end

      if @group.update(@add_member_params)
        json_response({ status: 'Success'})
      else
        render json: @group.errors, status: :unprocessable_entity
      end
    else
      raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
    end
  end

  #POST /groups/1/remove_member
  def remove_member
    #The logic is to call the update function but leave out the id that you want removed from the group
    @group = Group.find(params["id"].to_i)
      if @group.owner_id == current_user.id
      @current_member_params = {"id" => @group.id, "user_ids" => []}

      @current_member_ids = @group.user_ids
      @to_be_added = group_params["user_ids"]

      @current_member_ids.each do |current_member_id|
        @to_be_added.each do |user|
          if current_member_id != user
            @current_member_params["user_ids"] << current_member_id
          end
        end
      end

      if @group.update(@current_member_params)
        json_response({ status: 'Success'})
      else
        render json: @group.errors, status: :unprocessable_entity
      end
    else
      raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
    end
  end

  def ban_member
  	@user = User.find params[:user_id]
  	@group = Group.find params[:id]
  	if @group.users.include? @user
  		@group.users.delete @user
	  	if @group.banned_users.create({ user_id: @user.id })
	  		json_response({ status: 'Success'})
	  	else
	  		json_response({ status: 'error'})
	  	end
  	else
  		json_response({ status: 'error', message: 'This user is already banned from this group.' })
  	end  	
  end

  def silence_member
  	@user = User.find params[:user_id]
  	@group = Group.find params[:id]
  	@silence_time = DateTime.parse params[:silence_time]
  	if @group.users.include? @user
  		if @group.silence_users.collect{|suser| suser.user_id }.include? @user.id
  			if @group.silence_users.where({ user_id: @user}).first.update({ silence_time: @silence_time  })
		  		json_response({ status: 'Success'})
		  	else
		  		json_response({ status: 'error'})
		  	end  			
  		else
  			if @group.silence_users.create({ user_id: @user.id, silence_time: @silence_time  })
		  		json_response({ status: 'Success'})
		  	else
		  		json_response({ status: 'error'})
		  	end
  		end  		
  	else
  		json_response({ status: 'error', message: 'This user is not a member of this group.' })
  	end
  end

  # POST /groups
  def create
    @group = current_user.groups.build(group_params)
    @group.owner_id = current_user.id
    if @group.save
      render json: @group, serializer: GroupMinSerializer
    else
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /groups/1
  def update
    if @group.owner_id == current_user.id
      if @group.update(group_params)
        render json: @group
      else
        render json: @group.errors, status: :unprocessable_entity
      end
    else
      raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
    end
  end

  # DELETE /groups/1
  def destroy
    @group.destroy
    render json: @group
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def group_params
      params.require(:group).permit(:name, :avatar, :description, :inviteonly, :user_ids => [])
    end

  end
end
