module V1
  class PostsController < ApplicationController
    before_action :set_post, only: [:show, :update, :destroy]

  # GET /posts
  def index
    @posts = current_user.posts.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    render json: @posts
  end

  # GET /feed
  # TODO all the logic to pull a feed should be moved to the model.
  # Then we could Post.feed to test, etc.
  def feed
    # Get following user ids
    following_user_ids = current_user.following.pluck(:following_id)
    related_user_ids = ((following_user_ids) << current_user.id).uniq
    # Get none group posts
    @none_group_posts = Post.where(user_id: related_user_ids).where(group_id: nil)

    # Get user ids in all owner groups
    owner_group_ids = Group.select(:id).where(owner_id: current_user.id).ids
    # Get user ids in all joined groups
    joined_group_ids = Group.joins(:users).where("user_id = :user_id", user_id: current_user.id).ids
    # Merge related groups without duplication
    related_group_ids = owner_group_ids | joined_group_ids
    # Get my group posts
    @group_posts = Post.where(group_id: related_group_ids)

    # Merge posts
    @posts = @none_group_posts.or(@group_posts).order(created_at: :desc).paginate(page: params[:page], per_page: 10)

    render json: @posts, each_serializer: PostSerializer
  end

  def feed_group
  	if Group.exists? params[:id]
  		group = Group.find params[:id]
  	else
  		json_response(message: 'Invalid group')
  		return
  	end
	@posts = group.posts.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
  	render json: @posts, each_serializer: PostSerializer
  end

  def feed_recommend
    following_user_ids = current_user.following.pluck(:following_id)
    block_user_ids = Block.where(user_id: current_user.id).pluck(:block)
    remove_user_ids = (following_user_ids + block_user_ids)
    remove_user_ids << current_user.id
    @posts = Post.recommend.where.not(user_id: remove_user_ids).paginate(page: params[:page], per_page: 10)
    render json: @posts
  end

  # GET top posts /trending
  def trending
    block_user_ids = Block.where(user_id: current_user.id).pluck(:block)
    @posts = Post.trending.where.not(user_id: block_user_ids).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    render json: @posts
  end

  # GET /posts/1
  def show
    render json: @post
  end

  def user_posts
    @posts = User.find(params[:id]).posts.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    render json: @posts
  end

  # POST /posts
  def create
  	unless post_params[:group_id].nil?
  		if Group.exists? post_params[:group_id]
  			group = Group.find post_params[:group_id]
  		else
  			json_response(message: 'Invalid group')
  			return
  		end

      if group.owner_id != current_user.id
        unless group.users.include? current_user
          json_response(message: 'Invalid user for this group')
          return
        end
      end

  		if group.silence_users.collect{|suser| suser.user_id }.include? current_user.id
  			if Time.now < group.silence_users.where({ user_id: current_user.id }).first.silence_time
  				json_response(message: 'You are still a silence user for this group.')
  				return
  			end
  		end
  	end
    @post = current_user.posts.build(post_params)

    if @post.save
      # Send notifications in group members and followers
      PushNotification.notifyPost(current_user, @post)

      render json: @post
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def repost
  	unless Post.exists? post_params[:reference_post]
  		json_response(message: 'Invalid post')
  	else
  		@post = current_user.posts.build(post_params)

	    if @post.save
        # Send notifications in group members and followers
        PushNotification.notifyRepost(current_user, @post)

	      render json: @post
	    else
	      render json: @post.errors, status: :unprocessable_entity
	    end
  	end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      render json: @post
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    render json: @post
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:body, :group_id, :image, :reference_post)
    end
  end
end
