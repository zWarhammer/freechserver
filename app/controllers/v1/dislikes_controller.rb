module V1
  class DislikesController < ApplicationController
    before_action :set_dislike, only: [:show, :update, :destroy]

  # GET /dislikes
  def index
    @dislikes = current_user.order(created_at: :desc).dislikes.paginate(page: params[:page], per_page: 10)
    render json: @dislikes
  end

  # GET /dislikes/1
  def show
    render json: @dislike
  end

  # POST /dislikes
  def create
    #The logic is that if any of the current post's likes are from the current user,
    #don't allow them to like again
    @post = Post.find(dislike_params["post_id"].to_i)
    @post_dislikes = @post.dislikes
    @post_dislikes.each do |dislike|
      if dislike.user_id == current_user.id
        json_response({ status: 'Current User has already disliked this post'})
        return;
      end
    end


    @dislike = current_user.dislikes.build(dislike_params)

    if @dislike.save
      render json: @dislike
    else
      render json: @dislike.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dislikes/1
  def update
    if @dislike.update(dislike_params)
      render json: @dislike
    else
      render json: @dislike.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dislikes/1
  def destroy
    @dislike.destroy
    render json: @dislike
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dislike
      @dislike = Dislike.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dislike_params
      params.require(:dislike).permit(:post_id)
    end
  end
end
