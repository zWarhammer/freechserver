module V1
  class LikesController < ApplicationController
    before_action :set_like, only: [:show, :update, :destroy]

  # GET /likes
  def index
    @likes = current_user.likes.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    render json: @likes
  end

  # GET /likes/1
  def show
    render json: @like
  end

  # POST /likes
  def create
    #The logic is that if any of the current post's likes are from the current user,
    #don't allow them to like again
    #TODO can make this more direct and simple? Instead of going through all current likes, just see if a like with this user already exists.
    @post = Post.find(like_params["post_id"].to_i)
    @post_likes = @post.likes
    @post_likes.each do |like|
      if like.user_id == current_user.id
        json_response({ status: 'Current User has already liked this post'})
        return;
      end
    end

    @like = current_user.likes.build(like_params)

    if @like.save
      logger.info "Saving Like info. Should start Score job"
      begin
        PushNotification.notifyLike(current_user, @like)
      rescue StandardError => e
        puts "Failed to send notification to user with this error: #{e}"
      end
      ScorePostsJob.perform_later
      render json: @like
    else
      render json: @like.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /likes/1
  def update
    if @like.update(like_params)
      render json: @like
    else
      render json: @like.errors, status: :unprocessable_entity
    end
  end

  # DELETE /likes/1
  def destroy
    begin
      PushNotification.notifyDislike(current_user, @like)
    rescue StandardError => e
      puts "Failed to send notification to user with this error: #{e}"
    end
    @like.destroy
    render json: @like
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like
      @like = Like.find(params[:id])
    end
    # Only allow a trusted parameter "white list" through.
    def like_params
      params.require(:like).permit(:post_id)
    end
  end
end
