class PasswordController < ApplicationController
  skip_before_action :authorize_request, only: [:forgot, :reset]

  def forgot
    if params[:email].blank?
      return render json: {error: 'Email not present'}, status: :unprocessable_entity
    end

    user = User.find_by(email: params[:email])

    if user.present? #&& user.confirmed_at?
      user.generate_password_token!
      # SEND EMAIL HERE
      render json: {
        reset_password_token: user.reset_password_token,
        reset_password_sent_at: user.reset_password_sent_at
        }, status: :ok
    else
      render json: {error: ['Email address not found. Please check and try again.']}, status: :not_found
    end
  end


  def reset
    if params[:email].blank?
      return render json: {error: 'email not present'}, status: :unprocessable_entity
    end

    if params[:reset_password_token].blank?
      return render json: {error: 'Token not present'}, status: :unprocessable_entity
    end

    if params[:reset_password_sent_at].blank?
      return render json: {error: 'Token not present'}, status: :unprocessable_entity
    end
    
    if params[:new_password].blank?
      return render json: {message: 'New password not present'}, status: :unprocessable_entity
    end

    token = params[:reset_password_token].to_s

    user = User.find_by(reset_password_token: token)

    if user.present? && user.password_token_valid?
      if user.reset_password!(params[:new_password])
        render json: {status: 'ok'}, status: :ok
      else
        render json: {message: user.errors.full_messages}, status: :unprocessable_entity
      end
    else
      render json: {error: 'Link not valid or expired. Try generating a new link.'}, status: :not_found
    end
  end


  def update
    # Check valdiation of request
    if !params[:old_password].present?
      render json: {message: 'Old password not present'}, status: :unprocessable_entity
      return
    end

    if !params[:new_password].present?
      render json: {message: 'New password not present'}, status: :unprocessable_entity
      return
    end

    if !current_user || !current_user.authenticate(params[:old_password])
      render json: {message: 'Old password was mistaken'}, status: :unprocessable_entity
      return
    end

    # Update password
    if current_user.reset_password!(params[:new_password])
      render json: {status: 'ok'}, status: :ok
    else
      render json: {message: current_user.errors.full_messages}, status: :unprocessable_entity
    end
  end

end