class RegistrationsController < ActionController::Base
  before_action :allow_iframe_requests

  layout "layouts/application"

  # Sign up
  # <iframe src="https://3b11f587.ngrok.io/register" height="551" width="400"/>
  # Sign up and Join into group
  # <iframe src="https://3b11f587.ngrok.io/registrations/1/group" height="620" width="400"/>

  def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end

  # GET /register
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.email.downcase!

    if @user.save
      render :register_confirm, user: @user, notice: 'You was successfully registered.'
    else
      @error_messages = @user.errors
      render :new
    end
  end

  def register_confirm
  end

  def create_group
    # Register new user
    @user = User.new(user_params)
    @user.email.downcase!

    if @user.save
      # Add new user into group
      @group = Group.find(params[:id].to_i)
      add_member_params={user_ids: @user.id}
      if @group.update(add_member_params)
        render :join_confirm, user: @user, group: @group, notice: 'You was successfully registered and joined into group.'
      else
        @error_messages = @group.errors
        render :new
      end
    else
      @error_messages = @user.errors
      render :new
    end
  end

  def group
    @group = Group.where(id: params[:id]).first
    @user = User.new()
    if @group
      render :group
    else
      render :new
    end
  end

  def join_confirm
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :auth_token, :name, :username, :bio, :avatar, :push_token)
  end
end