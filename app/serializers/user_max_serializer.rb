class UserMaxSerializer < ActiveModel::Serializer
  attributes :id, :name, :bio, :email, :created_at, :updated_at, :posts, :comments, :likes, :dislikes, :groups, :avatar
  has_many :posts
  has_many :comments
  has_many :likes
  has_many :dislikes
  has_many :groups
end
