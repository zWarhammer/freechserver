class CommentSerializer < ActiveModel::Serializer
  attributes :id, :post_id, :body, :created_at, :updated_at, :user

  def user
    @user = User.find(object.user.id)
    @user_resp = Hash.new
    @user_resp["id"] = @user.id
    @user_resp["name"] = @user.name
    @user_resp["username"] = @user.username
    @user_resp["avatar"] = @user.avatar
    return @user_resp
  end

end
