class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :following, :follower, :username, :bio, :email, :created_at, :updated_at, :posts, :comments, :likes, :dislikes, :groups, :avatar, :follow, :push_token
  # has_many :posts
  # has_many :comments
  # has_many :likes
  # has_many :dislikes
  # has_many :groups
  # has_many :following
  # has_many :followers

  def posts
    object.posts.length
  end

  def comments
    object.comments.length
  end

  def likes
    object.likes.length
  end

  def dislikes
    object.dislikes.length
  end

  def groups
    object.groups.length
  end

  def following
    object.following.length
  end

  def follower
    object.follower.length
  end
end
