class FlagSerializer < ActiveModel::Serializer
  attributes :id, :reason, :contenttype, :flagged
  has_one :user
end
