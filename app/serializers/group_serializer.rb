class GroupSerializer < ActiveModel::Serializer
  attributes :id, :avatar, :name, :description, :owner, :users, :banned_users, :silence_users, :inviteonly
  has_many :users, serializer: UserMinSerializer

  def owner
    @owner = User.find(object.owner_id)
    @owner_resp = Hash.new
    @owner_resp["id"] = @owner.id
    @owner_resp["name"] = @owner.name
    @owner_resp["username"] = @owner.username
    return @owner_resp
  end
end
