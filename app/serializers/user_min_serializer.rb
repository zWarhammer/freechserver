class UserMinSerializer < ActiveModel::Serializer
  attributes :id, :name, :username, :avatar, :follow
end
