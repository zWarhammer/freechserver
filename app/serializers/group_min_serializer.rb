class GroupMinSerializer < ActiveModel::Serializer
  attributes :id, :avatar, :name, :owner_id, :users_length, :description, :inviteonly

  def users_length
    return object.users.length
  end
end
