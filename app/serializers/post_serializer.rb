class PostSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at, :updated_at, :likes, :dislikes, :comments, :image, :user, :comments_count, :likes_count, :dislikes_count, :reference
  belongs_to :user, serializer: UserMinSerializer
  #belongs_to :user
  has_many :comments, serializer: CommentSerializer
  #has_many :likes
  #has_many :dislikes
  has_one :reference, serializer: PostSerializer

  def comments
    block_user_ids = Block.where(user_id: object.user_id).pluck(:block)
    object.comments.where.not(user_id: block_user_ids)
  end

  def likes
    object.likes
  end

  def dislikes
    object.dislikes
  end

  def comments_count
    block_user_ids = Block.where(user_id: object.user_id).pluck(:block)
    comments = object.comments.where.not(user_id: block_user_ids)
    comments.length
  end

  def likes_count
    object.likes.length
  end

  def dislikes_count
    object.dislikes.length
  end

  def reference
  	object.reference
  end

end