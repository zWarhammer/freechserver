class ScorePostsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    logger.info "Starting Score Posts Job"
    posts = Post.all
    posts.each do |p|
      mult = 1.to_f
      td = Time.current-p.created_at
      if (td < 1.days)
        mult = 10
      elsif (td < 3.days)
        mult = 3
      elsif (td > 2.days and td < 7.days)
        mult = 2
      elsif (td > 13.days)
        mult = 0.5
      end
      c = p.comments.length*3
      p.score = (c + p.likes_count)*mult
      p.save
    end 
  end
end
