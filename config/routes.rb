Rails.application.routes.draw do

  resources :flags
  resources :blocks
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
	scope module: :v2, constraints: ApiVersion.new('v2') do
		resources :posts, only: :index
	end

	scope module: :v1, constraints: ApiVersion.new('v1', true) do
		resources :dislikes
		resources :likes
		resources :follows
		resources :comments
		resources :posts
		resources :groups do
			member do
				patch :add_member
				patch :remove_member
				patch :ban_member
				patch :silence_member
			end
			get :member, on: :collection
		end
	end

	resources :users do
    member do
      post :push_token
    end
  end

  resources :registrations do
    member do
      get :group
      post :create_group
    end
  end

  get '/register', to: 'registrations#new'
	get '/register_confirm', to: 'registrations#register_confirm'
	get '/join_confirm', to: 'registrations#join_confirm'

	get '/current_user', to: 'users#current'
	patch '/follow/:id' => 'users#follow', as: 'follow_profile'
	patch '/unfollow/:id' => 'users#unfollow', as: 'unfollow_profile'
	get '/followings/:id' => 'users#followings'
	get '/followers/:id' => 'users#followers'

	post 'auth/login', to: 'authentication#authenticate'
	post '/auth/login/twitter', to: 'authentication#authenticate_twitter'
  get '/trending' => 'v1/posts#trending'
	get '/feed' => 'v1/posts#feed'
	get '/feed/group/:id', to: 'v1/posts#feed_group'
	get '/feed/recommend', to: 'v1/posts#feed_recommend'
	post '/repost',    to: 'v1/posts#repost'
  get '/user_posts/:id' => 'v1/posts#user_posts'

  post 'password/forgot', to: 'password#forgot'
  post 'password/reset', to: 'password#reset'
  patch 'password/update', to: 'password#update'
end
